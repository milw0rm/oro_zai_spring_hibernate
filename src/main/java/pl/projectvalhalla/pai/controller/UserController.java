package pl.projectvalhalla.pai.controller;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.projectvalhalla.pai.entity.Passenger;
import pl.projectvalhalla.pai.entity.Pooling;
import pl.projectvalhalla.pai.service.PassengerService;
import pl.projectvalhalla.pai.service.PoolingService;
import pl.projectvalhalla.pai.service.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private PoolingService poolingService;
	
	@Autowired
	private PassengerService passengerService;

	@ModelAttribute("pooling")
	public Pooling constructPooling() {
		return new Pooling();
	}
 
	@RequestMapping("/account")
	public String account(Model model, Principal principal) {
		String name = principal.getName();
		model.addAttribute("user", userService.findOneWithPoolings(name));
		return "account";
	}

	@RequestMapping(value = "/account", method = RequestMethod.POST)
	public String addPooling(Model model, @Valid @ModelAttribute("pooling") Pooling pooling, BindingResult result,
			Principal principal) {
		if (result.hasErrors()) {
			return account(model, principal);
		}
		String name = principal.getName();
		poolingService.save(pooling, name);
		return "redirect:/account.html";
	}
	
	@RequestMapping("/pooling/remove/{id}")
	public String removePooling(@PathVariable int id) {
		Pooling pooling = poolingService.findOne(id);
		poolingService.delete(pooling);
		return "redirect:/account.html";
	} 
	
	@RequestMapping("/passenger/remove/{id}")
	public String removePassenger(@PathVariable int id) {
		Passenger passenger = passengerService.findOne(id);
		passengerService.delete(passenger);
		return "redirect:/account.html";
	}

}
