package pl.projectvalhalla.pai.controller;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.projectvalhalla.pai.entity.Passenger;
import pl.projectvalhalla.pai.entity.Pooling;
import pl.projectvalhalla.pai.service.PassengerService;
import pl.projectvalhalla.pai.service.PoolingService;

@Controller
@RequestMapping("/poolings")
public class PoolingController {

	@Autowired
	private PoolingService poolingService;
	
	@Autowired
	private PassengerService passengerService;
	
	@ModelAttribute("passenger")
	public Passenger constructPassenger() {
		return new Passenger();
	}
	
	@ModelAttribute("pooling")
	public Pooling constructPooling() {
		return new Pooling();
	}
	
	@RequestMapping
	public String poolings(Model model) {
		model.addAttribute("poolings", poolingService.findAll());
		return "poolings";
	}

	@RequestMapping("/{id}")
	public String detail(Model model, @PathVariable int id) {
		model.addAttribute("pooling", poolingService.findOneWithPassengers(id));
		return "pooling-detail";
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	public String addPassenger(Model model, @Valid @ModelAttribute("passenger") Passenger passenger, BindingResult result,
			Principal principal, Pooling pooling) {
		String name = principal.getName();
		passengerService.save(name, pooling);
		return "redirect:/poolings/{id}.html";
	}
	
	@RequestMapping("/poolings/passenger/remove/{id}")
	public String removePassenger(@PathVariable int id) {
		Passenger passenger = passengerService.findOne(id);
		passengerService.delete(passenger);
		return "redirect:/poolings.html";
	}

}
