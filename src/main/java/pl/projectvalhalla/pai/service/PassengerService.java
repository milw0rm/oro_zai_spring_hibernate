package pl.projectvalhalla.pai.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import pl.projectvalhalla.pai.entity.Passenger;
import pl.projectvalhalla.pai.entity.Pooling;
import pl.projectvalhalla.pai.entity.User;
import pl.projectvalhalla.pai.repository.PassengerRepository;
import pl.projectvalhalla.pai.repository.PoolingRepository;
import pl.projectvalhalla.pai.repository.UserRepository;

@Service
public class PassengerService {

	@Autowired
	private PassengerRepository passengerRepository;
	
	@Autowired
	private PoolingRepository poolingRepository;

	@Autowired
	private UserRepository userRepository;
	
	public void save(String name, Pooling pooling) {
		Passenger passenger = new Passenger();
		User user = userRepository.findByName(name);
		passenger.setPassenger(user);
		Pooling przejazd = poolingRepository.findById(pooling.getId());
		passenger.setPooling(przejazd);
		poolingRepository.save(przejazd);
		passengerRepository.save(passenger);	
	}

	public Passenger findOne(int id) {
		return passengerRepository.findOne(id);
	}
	
	@PreAuthorize("#passenger.passenger.name == authentication.name or hasRole('ROLE_ADMIN')")
	public void delete(@P("passenger") Passenger passenger){
		passengerRepository.delete(passenger);
	}

}
