package pl.projectvalhalla.pai.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import pl.projectvalhalla.pai.entity.Passenger;
import pl.projectvalhalla.pai.entity.Pooling;
import pl.projectvalhalla.pai.entity.Role;
import pl.projectvalhalla.pai.entity.User;
import pl.projectvalhalla.pai.repository.PassengerRepository;
import pl.projectvalhalla.pai.repository.PoolingRepository;
import pl.projectvalhalla.pai.repository.RoleRepository;
import pl.projectvalhalla.pai.repository.UserRepository;

@Service
@Transactional
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PoolingRepository poolingRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PassengerRepository passengerRepository;

	public List<User> findAll() {
		return userRepository.findAll();
	}

	public User findOne(int id) {
		return userRepository.findOne(id);
	}

	public void save(User user) {
		user.setEnabled(true);
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		user.setPassword(encoder.encode(user.getPassword()));

		List<Role> roles = new ArrayList<Role>();
		roles.add(roleRepository.findByName("ROLE_USER"));
		user.setRoles(roles);

		userRepository.save(user);
	}

	public User findOne(String username) {
		return userRepository.findByName(username);
	}

	@Transactional
	public User findOneWithPoolings(int id) {
		User user = findOne(id);
		List<Pooling> poolings = poolingRepository.findByDriver(user,
				new PageRequest(0, 2, Direction.DESC, "startPlaceDate"));
		for (Pooling pooling : poolings) {
			List<Passenger> passengers = passengerRepository.findByPooling(pooling);
			pooling.setPassengers(passengers);
		}
		user.setPoolings(poolings);
		return user;
	}

	public User findOneWithPoolings(String name) {
		User user = userRepository.findByName(name);
		return findOneWithPoolings(user.getId());
	}

	public void delete(int id) {
		userRepository.delete(id);
	}

}
