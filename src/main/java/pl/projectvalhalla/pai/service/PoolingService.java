package pl.projectvalhalla.pai.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import pl.projectvalhalla.pai.entity.Passenger;
import pl.projectvalhalla.pai.entity.Pooling;
import pl.projectvalhalla.pai.entity.User;
import pl.projectvalhalla.pai.repository.PassengerRepository;
import pl.projectvalhalla.pai.repository.PoolingRepository;
import pl.projectvalhalla.pai.repository.UserRepository;

@Service

public class PoolingService {
	
	@Autowired
	private PassengerRepository passengerRepository;
	
	@Autowired
	private PoolingRepository poolingRepository;

	@Autowired
	private UserRepository userRepository;
	
	public List<Pooling> findAll() {
		return poolingRepository.findAll();
	}
	
	public void save(Pooling pooling, String name) {
		User user = userRepository.findByName(name);
		pooling.setDriver(user);
		poolingRepository.save(pooling);
	}

	public Pooling findOne(int id) {
		return poolingRepository.findOne(id);
	}
	
	@Transactional
	public Pooling findOneWithPassengers(int id) {
		Pooling pooling = findOne(id);
		List<Passenger> passengers = passengerRepository.findByPooling(pooling);
		pooling.setPassengers(passengers);
		return pooling;
	}
	
	@PreAuthorize("$pooling.driver.name == authentication.name or hasRole('ROLE_ADMIN')")
	public void delete(@P("pooling") Pooling pooling){
		poolingRepository.delete(pooling);
	}

}
