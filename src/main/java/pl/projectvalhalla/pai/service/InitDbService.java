package pl.projectvalhalla.pai.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.projectvalhalla.pai.entity.Pooling;
import pl.projectvalhalla.pai.entity.Role;
import pl.projectvalhalla.pai.entity.User;
import pl.projectvalhalla.pai.repository.PoolingRepository;
import pl.projectvalhalla.pai.repository.RoleRepository;
import pl.projectvalhalla.pai.repository.UserRepository;

@Transactional
@Service
public class InitDbService {

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PoolingRepository poolingRepository;

	@PostConstruct
	public void init() {
		Role roleUser = new Role();
		roleUser.setName("ROLE_USER");
		roleRepository.save(roleUser);

		Role roleAdmin = new Role();
		roleAdmin.setName("ROLE_ADMIN");
		roleRepository.save(roleAdmin);

		User userAdmin = new User();
		userAdmin.setEnabled(true);
		userAdmin.setName("admin");
		userAdmin.setEmail("admin@projectvalhalla.pl");
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		userAdmin.setPassword(encoder.encode("admin"));
		List<Role> roles = new ArrayList<Role>();
		roles.add(roleAdmin);
		roles.add(roleUser);
		userAdmin.setRoles(roles);
		userRepository.save(userAdmin);
		
		User userUser = new User();
		userUser.setEnabled(true);
		userUser.setName("user");
		userUser.setEmail("user@projectvalhalla.pl");
		BCryptPasswordEncoder encoder2 = new BCryptPasswordEncoder();
		userUser.setPassword(encoder2.encode("user"));
		List<Role> roles2 = new ArrayList<Role>();
		roles2.add(roleUser);
		userUser.setRoles(roles2);
		userRepository.save(userUser);
		
		Pooling newPooling = new Pooling();
		newPooling.setStartPlaceName("Warszawa");
		newPooling.setFinishPlaceName("��d�");
		newPooling.setDriver(userAdmin);
		newPooling.setStartPlaceDate(new Date());
		poolingRepository.save(newPooling);
		
		Pooling newPooling2 = new Pooling();
		newPooling2.setStartPlaceName("Warszawa");
		newPooling2.setFinishPlaceName("��d�");
		newPooling2.setDriver(userAdmin);
		newPooling2.setStartPlaceDate(new Date());
		poolingRepository.save(newPooling2);
		
		Pooling newPooling3 = new Pooling();
		newPooling3.setStartPlaceName("Warszawa");
		newPooling3.setFinishPlaceName("��d�");
		newPooling3.setDriver(userAdmin);
		newPooling3.setStartPlaceDate(new Date());
		poolingRepository.save(newPooling3);
		
		Pooling newPooling4 = new Pooling();
		newPooling4.setStartPlaceName("Warszawa");
		newPooling4.setFinishPlaceName("��d�");
		newPooling4.setDriver(userAdmin);
		newPooling4.setStartPlaceDate(new Date());
		poolingRepository.save(newPooling4);
		
		Pooling newPooling5 = new Pooling();
		newPooling5.setStartPlaceName("Warszawa");
		newPooling5.setFinishPlaceName("��d�");
		newPooling5.setDriver(userAdmin);
		newPooling5.setStartPlaceDate(new Date());
		poolingRepository.save(newPooling5);
		
		Pooling newPooling6 = new Pooling();
		newPooling6.setStartPlaceName("Warszawa");
		newPooling6.setFinishPlaceName("��d�");
		newPooling6.setDriver(userAdmin);
		newPooling6.setStartPlaceDate(new Date());
		poolingRepository.save(newPooling6);
		
		Pooling newPooling7 = new Pooling();
		newPooling7.setStartPlaceName("Warszawa");
		newPooling7.setFinishPlaceName("��d�");
		newPooling7.setDriver(userAdmin);
		newPooling7.setStartPlaceDate(new Date());
		poolingRepository.save(newPooling7);
		
		Pooling newPooling8 = new Pooling();
		newPooling8.setStartPlaceName("Warszawa");
		newPooling8.setFinishPlaceName("��d�");
		newPooling8.setDriver(userAdmin);
		newPooling8.setStartPlaceDate(new Date());
		poolingRepository.save(newPooling8);
		
		Pooling newPooling9 = new Pooling();
		newPooling9.setStartPlaceName("Warszawa");
		newPooling9.setFinishPlaceName("��d�");
		newPooling9.setDriver(userAdmin);
		newPooling9.setStartPlaceDate(new Date());
		poolingRepository.save(newPooling9);
		
		Pooling newPooling10 = new Pooling();
		newPooling10.setStartPlaceName("Warszawa");
		newPooling10.setFinishPlaceName("��d�");
		newPooling10.setDriver(userAdmin);
		newPooling10.setStartPlaceDate(new Date());
		poolingRepository.save(newPooling10);
		
		Pooling newPooling11 = new Pooling();
		newPooling11.setStartPlaceName("Warszawa");
		newPooling11.setFinishPlaceName("��d�");
		newPooling11.setDriver(userAdmin);
		newPooling11.setStartPlaceDate(new Date());
		poolingRepository.save(newPooling11);
		
	}
}
