package pl.projectvalhalla.pai.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

@Entity
public class Pooling {

	@Id
	@GeneratedValue
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User driver;

	@OneToMany(mappedBy = "pooling", cascade=CascadeType.REMOVE)
	@Column(name="passenger")
	private List<Passenger> passengers;
	
	@Size(min=3, message = "Name must be at least 1 character!")
	private String startPlaceName;
	
	@Column(name = "startPlaceDate")
	private Date startPlaceDate;
	
	@Size(min=3, message = "Name must be at least 1 character!")
	private String finishPlaceName;
	
	private String luggageType;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public User getDriver() {
		return driver;
	}
	public void setDriver(User driver) {
		this.driver = driver;
	}
	public List<Passenger> getPassengers() {
		return passengers;
	}
	public void setPassengers(List<Passenger> passengers) {
		this.passengers = passengers;
	}
	public String getStartPlaceName() {
		return startPlaceName;
	}
	public void setStartPlaceName(String startPlaceName) {
		this.startPlaceName = startPlaceName;
	}
	public Date getStartPlaceDate() {
		return startPlaceDate;
	}
	public void setStartPlaceDate(Date startPlaceDate) {
		this.startPlaceDate = startPlaceDate;
	}
	public String getFinishPlaceName() {
		return finishPlaceName;
	}
	public void setFinishPlaceName(String finishPlaceName) {
		this.finishPlaceName = finishPlaceName;
	}
	public String getLuggageType() {
		return luggageType;
	}
	public void setLuggageType(String luggageType) {
		this.luggageType = luggageType;
	}

	
}
