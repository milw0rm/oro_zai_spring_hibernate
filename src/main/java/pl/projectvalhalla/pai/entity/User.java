package pl.projectvalhalla.pai.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

import pl.projectvalhalla.pai.annotation.UniqueUsername;

@Entity
public class User {

	@Id
	@GeneratedValue
	private Integer id;

	@Column(unique=true)
	@UniqueUsername(message = "U�ytkownik o takim loginie ju� istnieje!")
	@Size(min=3, message = "Login musi zawiera� przynajmniej 3 litery!")
	private String name;

	@Size(min = 1, message = "E-mail nie mo�e by� kr�tszy ni� 1 znak!")
	@Email(message = "Z�y format adresu e-mail!")
	private String email;
	
	@Size(min = 5, message = "Has�o musi mie� conajmniej 5 znak�w!")
	private String password;
	
	private boolean enabled;

	@ManyToMany
	@JoinTable
	private List<Role> roles;

	@OneToMany(mappedBy = "driver", cascade = CascadeType.REMOVE)
	private List<Pooling> poolings;
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public List<Pooling> getPoolings() {
		return poolings;
	}

	public void setPoolings(List<Pooling> poolings) {
		this.poolings = poolings;
	}

}
