package pl.projectvalhalla.pai.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Passenger {

	@Id
	@GeneratedValue
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User passenger;

	@ManyToOne
	@JoinColumn(name = "pooling_id")
	private Pooling pooling;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public User getPassenger() {
		return passenger;
	}

	public void setPassenger(User passenger) {
		this.passenger = passenger;
	}

	public Pooling getPooling() {
		return pooling;
	}

	public void setPooling(Pooling pooling) {
		this.pooling = pooling;
	}

}
