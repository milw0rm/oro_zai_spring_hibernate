package pl.projectvalhalla.pai.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.projectvalhalla.pai.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {
	Role findByName(String name);
}