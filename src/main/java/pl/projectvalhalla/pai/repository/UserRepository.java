package pl.projectvalhalla.pai.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.projectvalhalla.pai.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {
	User findByName(String name);
}