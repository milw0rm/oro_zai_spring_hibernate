package pl.projectvalhalla.pai.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.projectvalhalla.pai.entity.Passenger;
import pl.projectvalhalla.pai.entity.Pooling;

public interface PassengerRepository extends JpaRepository<Passenger, Integer> {
	List<Passenger> findByPooling(Pooling pooling);
}