package pl.projectvalhalla.pai.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import pl.projectvalhalla.pai.entity.Pooling;
import pl.projectvalhalla.pai.entity.User;

public interface PoolingRepository extends JpaRepository<Pooling, Integer> {
	List<Pooling> findByDriver(User driver, Pageable pageable);
	Pooling findById(Integer id);
}