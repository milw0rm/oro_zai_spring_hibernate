<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="../layout/taglib.jsp" %>

<script type="text/javascript">
$(document).ready(function() {
	$(".triggerRemove").click(function(e) {
		e.preventDefault();
		$("#modalRemove .removeBtn").attr("href", $(this).attr("href"));
		$("#modalRemove").modal();
	});
});
</script>

<table class="table table-bordered table-hover table-striped">
	<thead>
		<tr>
			<th>Nazwa użytkownika</th>
			<th>Operacje</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${poolings}" var="pooling">
			<tr>
				<td>
					<a href="<spring:url value="/poolings/${pooling.id}.html" />">
						<c:out value="${pooling.startPlaceName}" />
					</a>
				</td>
				<td>
					<a href="<spring:url value="/poolings/remove/${pooling.id}.html" />" class="btn btn-danger triggerRemove">
						Usuń
					</a>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>


<!-- Modal -->
<div class="modal fade" id="modalRemove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Usuń użytkownika</h4>
      </div>
      <div class="modal-body">
        Na pewno?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Rezygnuje</button>
        <a href="" class="btn btn-danger removeBtn">Usuń</a>
      </div>
    </div>
  </div>
</div>