<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="../layout/taglib.jsp"%>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$('.nav-tabs a:first').tab('show'); // Select first tab
						$(".triggerRemove").click(
								function(e) {
									e.preventDefault();
									$("#modalRemove .removeBtn").attr("href",
											$(this).attr("href"));
									$("#modalRemove").modal();
								});

						$(".poolingForm")
								.validate(
										{
											rules : {
												startPlaceName : {
													required : true,
													minlength : 3
												},
												startPlaceDate : {
													required : true,
													dateISO : true
												},
												finishPlaceName : {
													required : true,
													minlength : 3
												},
												luggage_type : {
													required : true
												}
											},
											highlight : function(element) {
												$(element).closest(
														'.form-group')
														.removeClass(
																'has-success')
														.addClass('has-error');
											},
											unhighlight : function(element) {
												$(element)
														.closest('.form-group')
														.removeClass(
																'has-error')
														.addClass('has-success');
											},
											messages : {
												startPlaceName : {
													required : "To pole jest wymagane!",
													minlength : "To pole musi zawierać minimum 3 znaki!"
												},
												finishPlaceName : {
													required : "To pole jest wymagane!",
													minlength : "To pole musi zawierać minimum 3 znaki!"
												},
												startPlaceDate : {
													required : "To pole jest wymagane!",
													dateISO : "Zły format daty!",
												},
												luggage_type : {
													required : "To pole jest wymagane!"
												}
											}
										});
					});
</script>


<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
	<li role="presentation" class="active"><a href="#poolings"
		aria-controls="poolings" role="tab" data-toggle="tab">Moje
			przejazdy</a></li>
	<li role="presentation"><a href="#profile" aria-controls="profile"
		role="tab" data-toggle="tab">Profil użytkownika</a></li>
	<li role="presentation"><a href="#messages"
		aria-controls="messages" role="tab" data-toggle="tab">Prywatne
			wiadomości</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
	<div role="tabpanel" class="tab-pane" id="profile">
		<h1>Witaj ${user.name}</h1>
		Twój adres e-mail to ${user.email}<br />
	</div>
	<div role="tabpanel" class="tab-pane active" id="poolings">
	<br />
		<!-- Button trigger modal -->
		<button type="button" class="btn btn-primary btn-lg"
			data-toggle="modal" data-target="#myModal">Dodaj przejazd</button>

		<form:form commandName="pooling"
			cssClass="form-horizontal poolingForm">
			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Dodaj przejazd</h4>
						</div>
						<div class="modal-body">

							<div class="form-group">
								<label for="name" class="col-sm-4 control-label">Miejsce
									startowe:</label>
								<div class="col-sm-8">
									<form:input path="startPlaceName" cssClass="form-control" />
									<form:errors path="startPlaceName" />
								</div>
							</div>

							<div class="form-group">
								<label for="name" class="col-sm-4 control-label">Data
									wyjazdu: (yyyy/mm/dd)</label>
								<div class="col-sm-8">
									<form:input path="startPlaceDate" cssClass="form-control" />
									<form:errors path="startPlaceDate" />
								</div>
							</div>


							<div class="form-group">
								<label for="name" class="col-sm-4 control-label">Miejsce
									docelowe:</label>
								<div class="col-sm-8">
									<form:input path="finishPlaceName" cssClass="form-control" />
									<form:errors path="finishPlaceName" />
								</div>
							</div>

							<div class="form-group">
								<label for="name" class="col-sm-4 control-label">Typ
									bagażu:</label>
								<div class="col-sm-8">

									<form:select path="luggageType" cssClass="form-control">
										<form:option value="Duży">Duży - 10zł</form:option>
										<form:option value="Średni">Średni - 8zł</form:option>
										<form:option value="Mały">Mały - 5zł</form:option>
									</form:select>
								</div>
							</div>


						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Rezygnuje</button>
							<input type="submit" class="btn btn-primary" value="Dodaj" />
						</div>
					</div>
				</div>
			</div>
		</form:form>

		<form:form commandName="passenger"
			cssClass="form-horizontal passengerForm">
			<!-- Modal -->
			<div class="modal fade" id="myModal2" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal2"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Dołącz do
								przejazdu</h4>
						</div>
						<div class="modal-body">Na pewno chcesz dołączyć do tego
							przejazdu?</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal2">Rezygnuje</button>
							<input type="submit" class="btn btn-primary" value="Dodaj" />
						</div>
					</div>
				</div>
			</div>
		</form:form>
		<br />
<ul class="list-group">
		<c:forEach items="${user.poolings}" var="pooling">
		
		<li class="list-group-item">
			<h2>
				<c:out value="${pooling.startPlaceName}" />
				-
				<c:out value="${pooling.finishPlaceName}" />
			</h2>
			<h3>
				<fmt:formatDate value="${pooling.startPlaceDate}"
					var="formattedDate" type="date" pattern="yyyy-MM-dd" />
				<c:out value="${formattedDate}" />
			</h3>
			<h4>
				Typ bagażu: <c:out value="${pooling.luggageType}" />
			</h4>

			<p>
				<a href="<spring:url value="/pooling/remove/${pooling.id}.html" />"
					class="btn btn-danger triggerRemove">Usuń przejazd</a>
			</p>
			
			

			<table class="table table-bordered table-hover table-striped">
				<thead>
					<tr>
						<th>Pasażerowie</th>
						<th>Opcje</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pooling.passengers}" var="passenger">
						<tr>
							<td><c:out value="${passenger.passenger.name}" /></td>
							<td><a
								href="<spring:url value="/passenger/remove/${passenger.id}.html" />"
								class="btn btn-danger triggerRemove">Usuń</a></td>
						</tr>

					</c:forEach>
				</tbody>
			</table>
			
			</li>
			
		</c:forEach></ul>

		<!-- Modal -->
		<div class="modal fade" id="modalRemove" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Usuń</h4>
					</div>
					<div class="modal-body">Na pewno?</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Rezygnuje</button>
						<a href="" class="btn btn-danger removeBtn">Usuń</a>
					</div>
				</div>
			</div>
		</div>


	</div>
	<div role="tabpanel" class="tab-pane active" id="messages"></div>
</div>




