<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="../layout/taglib.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		$('.nav-tabs a:first').tab('show'); // Select first tab
		$(".triggerRemove").click(function(e) {
			e.preventDefault();
			$("#modalRemove .removeBtn").attr("href", $(this).attr("href"));
			$("#modalRemove").modal();
		});
	});
</script>


<form:form commandName="passenger"
	cssClass="form-horizontal passengerForm">
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Dołącz</h4>
				</div>
				<div class="modal-body">
					Na pewno?
					<form:input path="passenger" value="${pooling.driver}"
						cssClass="form-control" style="display: none;" />
					<form:input path="pooling" value="${pooling}"
						 style="display: none;" cssClass="form-control" />
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Rezygnuje</button>
					<input type="submit" class="btn btn-primary" value="Dodaj" />
				</div>
			</div>
		</div>
	</div>
</form:form>


<h1>${pooling.startPlaceName}-${pooling.finishPlaceName}</h1>
<h2>
	<fmt:formatDate value="${pooling.startPlaceDate}" var="formattedDate"
		type="date" pattern="yyyy-MM-dd" />
	<c:out value="${formattedDate}" />
</h2>
<h3>
	Typ bagażu:
	<c:out value="${pooling.luggageType}" />
</h3>
<c:if
	test="${pooling.driver.name ne pageContext.request.userPrincipal.name}">
	<p>
		<!-- Button trigger modal -->
		<button type="button" class="btn btn-primary" data-toggle="modal"
			data-target="#myModal">Dołącz</button>
	</p>
</c:if>



<table class="table table-bordered table-hover table-striped">
	<thead>
		<tr>
			<th>Pasażerowie</th>
			<th>Rodzaj uczestnika</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><c:out value="${pooling.driver.name}" /></td>
			<td>Kierowca</td>
		</tr>
		<c:forEach items="${pooling.passengers}" var="passenger">
			<tr>
				<td><c:out value="${passenger.passenger.name}" /></td>
				<td>Pasażer <c:if
						test="${passenger.passenger.name eq pageContext.request.userPrincipal.name}">
						<a
								href="<spring:url value="poolings/passenger/remove/${passenger.id}.html" />"
								class="btn btn-danger triggerRemove">Usuń</a>
					</c:if>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="modalRemove" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Usuń przejazd</h4>
			</div>
			<div class="modal-body">Na pewno?</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Rezygnuje</button>
				<a href="" class="btn btn-danger removeBtn">Usuń</a>
			</div>
		</div>
	</div>
</div>

