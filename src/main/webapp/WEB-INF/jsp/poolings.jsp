<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="../layout/taglib.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		//var auto = setInterval(    function ()
			//     {
			  //        $('#score').load('poolings.html').fadeIn("slow");
			    // }, 5000); // refresh every 5000 milliseconds
		$(".triggerRemove").click(function(e) {
			e.preventDefault();
			$("#modalRemove .removeBtn").attr("href", $(this).attr("href"));
			$("#modalRemove").modal();
		});
	});
</script>

<c:choose>
	<c:when test="${empty poolings}">
        Brak przejazdów.
    </c:when>
	<c:otherwise>
		<table class="table table-bordered table-hover table-striped">
			<thead>
				<tr>
					<th>Przejazd</th>
					<th>Data</th>
					<th>Typ bagażu</th>
					<th>Kierowca</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${poolings}" var="pooling">
					<tr>
						<td><a
							href="<spring:url value="/poolings/${pooling.id}.html" />"> <c:out
									value="${pooling.startPlaceName}" /> - <c:out
									value="${pooling.finishPlaceName}" />
						</a></td>
						<td><fmt:formatDate value="${pooling.startPlaceDate}"
								var="formattedDate" type="date" pattern="yyyy-MM-dd" /> <c:out
								value="${formattedDate}" /></td>
						<td><c:out value="${pooling.luggageType}" /></td>
						<td><a
							href="<spring:url value="/users/${pooling.driver.id}.html" />">
								<c:out value="${pooling.driver.name}" />
						</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</c:otherwise>
</c:choose>





<!-- Modal -->
<div class="modal fade" id="modalRemove" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Usuń użytkownika</h4>
			</div>
			<div class="modal-body">Na pewno?</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Rezygnuje</button>
				<a href="" class="btn btn-danger removeBtn">Usuń</a>
			</div>
		</div>
	</div>
</div>