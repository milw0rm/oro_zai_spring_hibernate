<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="../layout/taglib.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		$('.nav-tabs a:first').tab('show'); // Select first tab
		$(".triggerRemove").click(function(e) {
			e.preventDefault();
			$("#modalRemove .removeBtn").attr("href", $(this).attr("href"));
			$("#modalRemove").modal();
		});
	});
</script>

<h1>Użytkownik ${user.name} i jego przejazdy:</h1>
	<ul class="list-group">
<c:forEach items="${user.poolings}" var="pooling">

		<li class="list-group-item">
			<h2>
				<c:out value="${pooling.startPlaceName}" />
				-
				<c:out value="${pooling.finishPlaceName}" />
			</h2>
			<h3>
				<fmt:formatDate value="${pooling.startPlaceDate}"
					var="formattedDate" type="date" pattern="yyyy-MM-dd" />
				<c:out value="${formattedDate}" />
			</h3>
			<h4>
				Typ bagażu:
				<c:out value="${pooling.luggageType}" />
			</h4> <c:if
				test="${pooling.driver.name eq pageContext.request.userPrincipal.name}">
				<p>
					<a href="<spring:url value="/pooling/remove/${pooling.id}.html" />"
						class="btn btn-danger triggerRemove">Usuń przejazd</a>
				</p>
			</c:if>

			<table class="table table-bordered table-hover table-striped">
				<thead>
					<tr>
						<th>Pasażerowie</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pooling.passengers}" var="passenger">
						<tr>
							<td><c:out value="${passenger.passenger.name}" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</li>





</c:forEach>

</ul>

<!-- Modal -->
<div class="modal fade" id="modalRemove" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Usuń przejazd</h4>
			</div>
			<div class="modal-body">Na pewno?</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Rezygnuje</button>
				<a href="" class="btn btn-danger removeBtn">Usuń</a>
			</div>
		</div>
	</div>
</div>