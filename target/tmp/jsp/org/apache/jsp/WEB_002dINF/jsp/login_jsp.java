package org.apache.jsp.WEB_002dINF.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("<style>\n");
      out.write(".form-signin {\n");
      out.write("\tmax-width: 330px;\n");
      out.write("\tpadding: 15px;\n");
      out.write("\tmargin: 0 auto;\n");
      out.write("}\n");
      out.write(".form-signin .form-signin-heading,.form-signin .checkbox {\n");
      out.write("\tmargin-bottom: 10px;\n");
      out.write("}\n");
      out.write(".form-signin .checkbox {\n");
      out.write("\tfont-weight: normal;\n");
      out.write("}\n");
      out.write(".form-signin .form-control {\n");
      out.write("\tposition: relative;\n");
      out.write("\theight: auto;\n");
      out.write("\t-webkit-box-sizing: border-box;\n");
      out.write("\t-moz-box-sizing: border-box;\n");
      out.write("\tbox-sizing: border-box;\n");
      out.write("\tpadding: 10px;\n");
      out.write("\tfont-size: 16px;\n");
      out.write("}\n");
      out.write(".form-signin .form-control:focus {\n");
      out.write("\tz-index: 2;\n");
      out.write("}\n");
      out.write(".form-signin input[type=\"email\"] {\n");
      out.write("\tmargin-bottom: -1px;\n");
      out.write("\tborder-bottom-right-radius: 0;\n");
      out.write("\tborder-bottom-left-radius: 0;\n");
      out.write("}\n");
      out.write(".form-signin input[type=\"password\"] {\n");
      out.write("\tmargin-bottom: 10px;\n");
      out.write("\tborder-top-left-radius: 0;\n");
      out.write("\tborder-top-right-radius: 0;\n");
      out.write("}\n");
      out.write("</style>\n");
      out.write("\n");
      out.write("<form class=\"form-signin\" role=\"form\" action=\"/j_spring_security_check\" method=\"POST\">\n");
      out.write("\t<h2 class=\"form-signin-heading\">Logowanie</h2>\n");
      out.write("\t<input type=\"text\" name=\"j_username\" class=\"form-control\" placeholder=\"Login\" required autofocus> \n");
      out.write("\t<input type=\"password\" name=\"j_password\" class=\"form-control\" placeholder=\"Hasło\" required> \n");
      out.write("\t<button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Zaloguj</button>\n");
      out.write("</form>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
